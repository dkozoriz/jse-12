package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    public Task findOneById(final String id) {
        for (final Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findOneByIndex(Integer index) {
        return tasks.get(index);
    };

    public int getSize() {
        return tasks.size();
    }

    public void remove(Task task) {
        if (task == null) return;
        tasks.remove(task);
    }

    public Task removeById(String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    public Task removeByIndex(Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

}