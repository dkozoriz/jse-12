package ru.t1.dkozoriz.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showVersion();

    void showAbout();

    void showHelp();

    void showErrorArgument();

    void showErrorCommand();

}