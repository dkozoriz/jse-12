package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.ICommandRepository;
import ru.t1.dkozoriz.tm.api.service.ICommandService;
import ru.t1.dkozoriz.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}