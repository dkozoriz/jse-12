package ru.t1.dkozoriz.tm;

import ru.t1.dkozoriz.tm.component.Bootstrap;

public class Application {

    public static void main(final String... args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}